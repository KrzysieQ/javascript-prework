{
	const getMoveName = function(argMoveId) {
		if(argMoveId == 1) {
			return 'kamień';
		} else if(argMoveId == 2) {
			return 'papier';
		} else if(argMoveId == 3) {
			return 'nożyce';
		} else {
			printMessage('Nie znam ruchu o id ' + argMoveId + '.');
			return 'nieznany ruch';
		}
	}

	const displayResult = function(argComputerMove, argPlayerMove) {
		if(argComputerMove == argPlayerMove || (argComputerMove == 'nieznany ruch' || argPlayerMove == 'nieznany ruch')) {
			printMessage('Remis! ' + argComputerMove);
		} else if(argComputerMove == 'kamień' && argPlayerMove == 'papier') {
			printMessage('Ty wygrywasz! ' + argComputerMove);
		} else if(argComputerMove == 'papier' && argPlayerMove == 'nożyce') {
			printMessage('Ty wygrywasz! ' + argComputerMove);
		} else if(argComputerMove == 'nożyce' && argPlayerMove == 'kamień') {
			printMessage('Ty wygrywasz! ' + argComputerMove);
		} else {
			printMessage('Przegrywasz! ' + argComputerMove);
		}
	}

	const playGame = function(playerMove) {
		let randomNumber = Math.floor(Math.random() * 3 + 1);
		let computerMove = getMoveName(randomNumber);
		playerMove = getMoveName(playerMove);
		clearMessages();
		displayResult(computerMove, playerMove);
	}

	document.getElementById('play-rock').addEventListener('click', function(){
		playGame(1);
	});

	document.getElementById('play-paper').addEventListener('click', function(){
		playGame(2);
	});

	document.getElementById('play-scissors').addEventListener('click', function(){
		playGame(3);
	});
}